# Project Setup Instructions

Follow these steps to set up the project on your local machine.

## Step 1: Create the Project Folder

Open your terminal and execute the following commands to create a folder for the project and navigate into it:
```
mkdir test 
cd test
```
## Step 2: Clone the repository
Clone the given repository
```
git clone git@gitlab.com:shoplikoval/test.git
```
## Step 3: Installing Nginx
Install the Nginx to  your machine
```
sudo apt update
sudo apt install nginx
```

## Step 4: Setting up webpage
Here index.html file is copied to the directory, where your page will be stored
```
sudo mkdir /var/www/test
sudo cp index.html /var/www/test
```
## Step 5: Setting up the host by copying config file
The Nginx configuration file is copied to the sites-enabled directory
```
sudo cp test /etc/nginx/sites-enabled/
```
## Step 6: Restarting the Nginx server
After restarting the server with the command below, go to the ```localhost:81``` in your browser
```
sudo service nginx restart
```

## Step 7: To run the docker container of the webpage
You should be on the project's directory, where you should run the following commands
```
docker build -t server .  
docker run -it --rm -d -p 8080:80 --name web server
```
The docker container will run on the ```localhost:8080```, built on the Dockerfile of project
